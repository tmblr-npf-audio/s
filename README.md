### tumblr_npf_audio() plugin

**Table of Contents:**
- [About](#about)
- [Preview](#preview)
- [How to install](#how-to-install)
- [Usage notes](#usage-notes)
- [Credits](#credits)
- [Need help?](#issues--troubleshooting)
- [Thank you!](#found-this-useful)

---

#### About:
- **Context:** Tumblr introduced a new post format (NPF) a few years back, and since then, they've been working on replacing the existing post format system (which had various post types) with NPF, which is considered as a *text post*. Elements that aren't text, such as audio snippets and embeds, are still under development and unstyled, and differ from how they used to appear in the older post format. As of [July 5th 2023](https://changes.tumblr.com/post/722041241980846080), Tumblr has added the audio's details as metadata to its HTML markup.
- **What this does:** This plugin targets all NPF audio instances and adapts their styling to fit the legacy audio appearance on Tumblr's default theme.
- **Features:**
  - play/pause buttons with customizable sizes and colors
  - [optional] preceding labels, e.g. "Track name: *the actual track name*"
  - [optional] placeholder text for empty fields, e.g. "Untitled Track" / "Untitled Artist"
  - shows the audio's album image if it comes with one, + customizable dimensions
  - customizable player padding and background color
  - autoplays the *next* song if there is one.
- **Requirements:** basic CSS knowledge is a plus.
- **Editing & Customization:** you're welcome to `inspect element` as much as you want to get an overview of the markup, to see what the class names are if you want to customize any of them.
- **Author:** HT (@&#x200A;glenthemes)
- **View on Tumblr:** [glenthemes.tumblr.com/post/722160746171072512](https://glenthemes.tumblr.com/post/722160746171072512)

#### Preview:
![A rendered audio player with a light gray background. A play button sits on the left, followed by the audio's details, including the track name, artist name, and album name, all with a corresponding descriptive label. The audio's album cover image sits on the very right.](https://static.tumblr.com/snld4jp/Nc1sbr6km/screenshot_2023-07-06_at_8.43.05_pm.png)

---

#### How to install:
Paste the following in **either** of these locations:
- between `<head>` and `</head>`, *or*
- between `<body>` and `</body>`
```html
<!--✻✻✻✻✻✻  npf audio player by @glenthemes  ✻✻✻✻✻✻-->
<script src="//tmblr-npf-audio.gitlab.io/s/init.js"></script>
<link href="//tmblr-npf-audio.gitlab.io/s/base.css" rel="stylesheet">
<script>
tumblr_npf_audio({
    emptyTitleText: "Untitled track",
    emptyArtistText: "Untitled artist",
    emptyAlbumText: "Untitled album",
    
    titleLabel: "Track:",
    artistLabel: "Artist:",
    albumLabel: "Album:"
});
</script>
<style edit-npf-audio-player>
.npf-audio-wrapper {
    --NPF-Audio-Buttons-Size: 1.4rem;
    --NPF-Audio-Buttons-Color: #555555; /* color of play & pause buttons */
    --NPF-Audio-Buttons-Spacing: 1.3rem; /* space between buttons and song info */
    
    --NPF-Audio-Image-Size: 85px;
    --NPF-Audio-Image-Spacing: 0px; /* gap between player info and cover image */
}

.npf-audio-background {
    background-color: #f2f2f2; /* background color of player (optional) */
    padding: 1.5rem;
}

.npf-audio-title-label,
.npf-audio-artist-label,
.npf-audio-album-label {
    font-weight: bold;
}

.npf-audio-title,
.npf-audio-artist,
.npf-audio-album {
    color: #333333; /* color of audio text (optional) */
}
</style>
```
---

#### Usage notes:
If you don't want any of the options listed in `tumblr_npf_audio`, you can leave them empty with quotes. Here's an example:
```javascript
tumblr_npf_audio({
    emptyTitleText: "",
    emptyArtistText: "",
    emptyAlbumText: "",
    
    titleLabel: "",
    artistLabel: "",
    albumLabel: ""
});
```
Do not remove any commas!

Since this plugin uses JavaScript, there will be a very brief delay where you will see the default player before the plugin kicks in. If this bothers you, you can add the following CSS; paste it under `<style edit-npf-audio-player>`:
```css
figcaption.audio-caption,
figcaption.audio-caption + audio[controls]{
    display:none!important;
}
```

---

#### Credits:
- play icon: IYAHICON @ Flaticon [[🔗](https://dub.sh/AAu5LnA)]
- pause icon: Chanut @ Flaticon [[🔗](https://dub.sh/ecG8aE9)]

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this useful?
Consider leaving me a tip over at [ko-fi.com/glenthemes](https://ko-fi.com/glenthemes)!  
Thank you :sparkles:
