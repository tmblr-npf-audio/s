/*----------------------------------------------
    
    tumblr npf audio player
    - written by HT (@glenthemes)

    Last updated: 2023/07/06
    
    Credits:
    - play icon: IYAHICON @ Flaticon
      https://dub.sh/AAu5LnA
    - pause icon: Chanut @ Flaticon
      https://dub.sh/ecG8aE9
    
----------------------------------------------*/

window.tumblr_npf_audio = function(e_e){
document.addEventListener("DOMContentLoaded", () => {
    let aud_figcapt = document.querySelectorAll("figcaption.audio-caption");
    aud_figcapt ? aud_figcapt.forEach(fig => {
        fig.classList.remove("audio-caption");
        fig.classList.add("npf-audio-wrapper")
        if(fig.parentNode.matches(".tmblr-full")){
            fig.parentNode.classList.remove("tmblr-full")
        }
        
        /*---- audio details ----*/
        let dts = fig.querySelector(".tmblr-audio-meta.audio-details");
        if(dts){
            let dznts = ["tmblr-audio-meta", "audio-details"];
            for(let nt of dznts){
                dts.classList.remove(nt)
            }
            dts.classList.add("npf-audio-details")
            
            dts.querySelectorAll(".tmblr-audio-meta").forEach(meta => {
                meta.classList.remove("tmblr-audio-meta");
                switch(true){
                    case meta.classList.contains("title"):
                         meta.classList += " npf-audio-title";
                         meta.classList.remove("title");
                         if(meta.textContent.trim() == "" && (e_e.emptyTitleText || typeof(e_e.emptyTitleText) !== "undefined" || e_e.emptyTitleText.trim() !== "")){
                            meta.textContent = e_e.emptyTitleText
                         }
                         if(e_e.titleLabel || typeof(e_e.titleLabel) !== "undefined" || e_e.titleLabel.trim() !== ""){
                             let span1 = document.createElement("span");
                             span1.classList.add("npf-audio-title-label");
                             span1.textContent = e_e.titleLabel + " ";
                             meta.prepend(span1)
                         }
                         break;
                         
                    case meta.classList.contains("artist"):
                         meta.classList += " npf-audio-artist";
                         meta.classList.remove("artist");
                         if(meta.textContent.trim() == "" && (e_e.emptyArtistText || typeof(e_e.emptyArtistText) !== "undefined" || e_e.emptyArtistText.trim() !== "")){
                            meta.textContent = e_e.emptyArtistText
                         }
                         if(e_e.artistLabel || typeof(e_e.artistLabel) !== "undefined" || e_e.artistLabel.trim() !== ""){
                             let span2 = document.createElement("span");
                             span2.classList.add("npf-audio-artist-label");
                             span2.textContent = e_e.artistLabel + " ";
                             meta.prepend(span2)
                         }
                         break;
                         
                    case meta.classList.contains("album"):
                         meta.classList += " npf-audio-album";
                         meta.classList.remove("album");
                         if(meta.textContent.trim() == "" && (e_e.emptyAlbumText || typeof(e_e.emptyAlbumText) !== "undefined" || e_e.emptyAlbumText.trim() !== "")){
                            meta.textContent = e_e.emptyAlbumText
                         }
                         if(e_e.albumLabel || typeof(e_e.albumLabel) !== "undefined" || e_e.albumLabel.trim() !== ""){
                             let span3 = document.createElement("span");
                             span3.classList.add("npf-audio-album-label");
                             span3.textContent = e_e.albumLabel + " ";
                             meta.prepend(span3)
                         }
                         break;
                }
                
                
            })
        }
        
        /*---- audio cover image ----*/
        let cov = fig.parentNode.querySelector("img.album-cover");
        if(cov){
            cov.classList.remove("album-cover");
            cov.classList.add("npf-audio-image");
        }
        
        /*---- controls ----*/
        let rtz = document.createElement("div");
        rtz.classList.add("npf-audio-buttons");
        fig.prepend(rtz);
        
        let playstr = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path fill-rule="evenodd" d="M468.8 235.007 67.441 3.277A24.2 24.2 0 0 0 55.354-.008h-.07A24.247 24.247 0 0 0 43.19 3.279a24 24 0 0 0-12.11 20.992v463.456a24.186 24.186 0 0 0 36.36 20.994L468.8 276.99a24.238 24.238 0 0 0 0-41.983z" fill="var(--NPF-Audio-Buttons-Color)"></path></g></svg>';
        
        let rtPlay = document.createElement("div");
        rtPlay.classList.add("npf-audio-play");
        rtz.append(rtPlay);
        rtPlay.innerHTML = playstr;
        
        let pausestr = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="512" height="512" x="0" y="0" viewBox="0 0 47.607 47.607" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path d="M17.991 40.976a6.631 6.631 0 0 1-13.262 0V6.631a6.631 6.631 0 0 1 13.262 0v34.345zM42.877 40.976a6.631 6.631 0 0 1-13.262 0V6.631a6.631 6.631 0 0 1 13.262 0v34.345z" fill="var(--NPF-Audio-Buttons-Color)" class=""></path></g></svg>'
        
        let rtPause = document.createElement("div");
        rtPause.classList.add("npf-audio-pause");
        rtPause.style.display = "none";
        rtz.append(rtPause);
        rtPause.innerHTML = pausestr;
        
        /*---- separate left from right ---*/
        let npf_audio_left = document.createElement("div");
        npf_audio_left.classList.add("npf-audio-background");
        fig.prepend(npf_audio_left);
        
        if(npf_audio_left.nextElementSibling.matches(".npf-audio-buttons")){
            npf_audio_left.append(npf_audio_left.nextElementSibling)
        }
        
        if(npf_audio_left.nextElementSibling.matches(".npf-audio-details")){
            npf_audio_left.append(npf_audio_left.nextElementSibling)
        }
        
        /*---- audio itself ----*/
        let aud = fig.nextElementSibling;
        if(aud && aud.matches("audio")){
            aud.hidden = "";
            aud.style.display = "none"
            
            rtPlay.addEventListener("click", () => {
                if(aud.paused){
                    aud.play()
                }
            })
            
            rtPause.addEventListener("click", () => {
                if(!aud.paused){
                    aud.pause();
                }
            })
            
            aud.addEventListener("play", () => {
                rtPlay.style.display = "none";
                rtPause.style.display = "block"
            })
            
            aud.addEventListener("pause", () => {
                rtPlay.style.display = "block";
                rtPause.style.display = "none"
            })
            
            aud.addEventListener("ended", () => {
                rtPlay.style.display = "block";
                rtPause.style.display = "none"
            })
        }
    }) : ""
})//end DOM loaded
}//end function
